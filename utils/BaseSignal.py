from PyQt5 import QtCore


class BaseSignal:
    signalGetStockToWidget = QtCore.pyqtSignal(int, int, str)
    signalAddStockToWidget = QtCore.pyqtSignal(int, int, str)
    signalSetStockWidget = QtCore.pyqtSignal(int, int, list)
    signalUpdateProgressBar = QtCore.pyqtSignal(int)
    signalShowProgressBar = QtCore.pyqtSignal()
    signalShowPermMessage = QtCore.pyqtSignal(str)
    signalShowMessage = QtCore.pyqtSignal(str)
    signalFinished = QtCore.pyqtSignal()
