# 获取帮助

在开发和使用[Heaven_Hand_stock](https://gitee.com/lawrence2012/Heaven_Hand_stock/)项目的过程中遇到问题时，获取帮助的渠道包括：

- Github Issues：[Issues页面](https://gitee.com/lawrence2012/Heaven_Hand_stock/issues)
- 官方QQ群: 2214927852
![输入图片说明](img/image.png)
- 项目邮箱: [hu3692@163.com]()